// 1) - setTimeout() дозволяє викликати функцію один раз через якийсь проміжок часу,
// а setInterval() викликає функцію циклічно, повторюючи її виклик через встановлений інтервал часу.
// 2) - якщо передати "0" в затримку, то функція буде не буде викликана миттєво, але буде запланована інтерпретатором наскільки швидко, наскільки це можливо і
// буде виконана відразу після виконання іншого коду
// 3) Важливість полягає у очищенні таймера, який був заданий у setInterval, що відміняє регулярність виконання функції і зменшує навантаження на систему

let i = 1
let intervalTime = 3000
const stopButton = document.createElement("button")
stopButton.innerHTML = "Припинити"
stopButton.style.backgroundColor = "red"
stopButton.style.cursor = "pointer"
stopButton.style.marginRight = "10px"
document.body.append(stopButton)
stopButton.addEventListener("click", () => {
  clearInterval(showImg);
  showImg = null
})
const startButton = document.createElement("button")
startButton.innerHTML = "Продовжити"
startButton.style.backgroundColor = "green"
startButton.style.cursor = "pointer"
stopButton.after(startButton)
startButton.addEventListener("click", () => {
  if (showImg === null) {
    showImg = setInterval(showCurrentImg, intervalTime)
  }
})

let showImg = setInterval(showCurrentImg, intervalTime)
showCurrentImg()
function showCurrentImg() {
  if (i === 5) {
  i = 1
}
const img = document.querySelectorAll("img")
img.forEach((img) => {
  if (i == img.id) {
    img.style.display = "block"
  } else {
    img.style.display = "none"
  }
})
i++
}