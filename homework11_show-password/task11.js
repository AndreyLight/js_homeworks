const showPass = document.querySelectorAll(".fas");
const iInput = document.getElementById("input-password");
const iSubmit = document.getElementById("submit-password");
const button = document.querySelector(".btn");

const warningDiv = document.createElement("div");
warningDiv.innerHTML = "Потрібно ввести однакові значення";
warningDiv.style.color = "red";
iSubmit.append(warningDiv);
warningDiv.classList.add("no-view");

showPass.forEach((e) => {
  e.addEventListener("click", () => {
    const input = e.closest(".input-wrapper").querySelector("input");
    if (input.getAttribute("type") === "text") {
      input.setAttribute("type", "password");
    } else {
      input.setAttribute("type", "text");
    }
  });
});

iInput.addEventListener("click", (event) => {
  if (event.target.className === "fas fa-eye icon-password input") {
    event.target.classList.replace("fa-eye", "fa-eye-slash");
  } else {
    event.target.classList.replace("fa-eye-slash", "fa-eye");
  }
});

iSubmit.addEventListener("click", (event) => {
  if (event.target.className === "fas fa-eye icon-password submit") {
    event.target.classList.replace("fa-eye", "fa-eye-slash");
  } else {
    event.target.classList.replace("fa-eye-slash", "fa-eye");
  }
});

button.addEventListener("click", (evt) => {
  const inputText = document.getElementById("input");
  const submitInputText = document.getElementById("submit");
  evt.preventDefault();
  if (inputText.value === submitInputText.value) {
    inputText.value = "";
    submitInputText.value = "";
    alert("You are welcome");
    warningDiv.classList.remove("active");
  } else {
    warningDiv.classList.add("active");
  }
});
