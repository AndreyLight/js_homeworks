// 1 - Функція - це підпрограма (оремо виділений блок коду), яка може бути викликана безліч кількість разів.
// Її призначення - зробити код більш зручним, щоб не повторювати одні і ті ж команди або інструкції декілька разів.
// 2 - Вони потрібні щоб передати якесь необхідне значення функції при її безпосередньому виклиці замість встановлених параметрів в тілі самої функції. 
// 3 - return завершує виконання функції і повертає якесь значення в місце виклику фунції

function mathOperation(a, b, userFunction) {
  a = +prompt("Enter number a");

  while (!isValidNumber(a)) {
    a = +prompt("Enter valid number a again");
    }

  b = +prompt("Enter number b");

  while (!isValidNumber(b)) {
    b = +prompt("Enter valid number b again");
    }

  userFunction = prompt("Enter your function");
  
  while (!isValidFunction(userFunction)) {
    userFunction = prompt("Fuction is incorrect. Enter your function again!")
  } 

  function isValidNumber(value) {
    if (value === null || value === "" || Number.isNaN(+value)) {
      return false;
    }
    return true;
  }

  function isValidFunction(value) {
    if (value == "+" || value == "-" || value == "*" || value == "/") {
      return true;
    }
    return false;
  }

  if (userFunction === "+") {
    console.log(a + b)
  } else if (userFunction === "-") {
    console.log(a - b)
  } else if (userFunction === "*") {
    console.log(a * b)
  } else if (userFunction === "/") {
    console.log(a / b)
  }
  }

mathOperation()