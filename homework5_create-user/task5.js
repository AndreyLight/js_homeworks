// 1 - Метод об'єкта - це функція об'єкта (дії, які об'єкт може виконувати).  
// 2 - Будь-який тип данних може бути збережений в ключі об'єкта
// 3 Наприклад, в obj.objkey буде повертатись ссилочне значення ключа і об'єкт з якого воно взяте. В змінних ссилочних типів зберігаються ссилки на їх об'єкти. Дві змінні ссилочного типу можуть зветатись до того самого об'єкту.
function createUser() {
   const userName = prompt("Whats your first name?")
   const userLastName = prompt("Whats your last name?")
   const person = {
    firstName:userName,
    lastName:userLastName,
    getlogin(){
        return this.firstName.charAt(0).toLowerCase() + this.lastName.toLowerCase()
    }
   }
   Object.defineProperty(person, 'firstName', {
    value: userName,
    writable : false
   })

   return person
} 
const newUser = createUser()

console.log(newUser)
console.log(newUser.getlogin())