let tabs = document.querySelectorAll(".tabs .tabs-title");
tabs.forEach((element) => {
    element.addEventListener("click", () => {
    const char = element.dataset.id;
    const textLi = document.querySelectorAll(".tabs-content li");
   
    const removeActive = document.querySelectorAll(".tabs-title")
    removeActive.forEach((i) => {i.classList.remove("active")})

    element.classList.add("active")
    
    textLi.forEach((e) => {
        if (char === e.dataset.content) {
            e.classList.add("show-text")
        } else {
        e.classList.remove("show-text")
        }
    })
  });
});
