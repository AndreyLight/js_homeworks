const themeStorage = localStorage.getItem("theme")
const lightTheme = function () {
    themeBtn.innerHTML = "Dark";
    themeBtn.classList.replace("dark", "light");
    document.body.style.backgroundColor = "white";
    themeBtn.style.color = "white";
    themeBtn.style.backgroundColor = "black";
    document.querySelector(".container").style.backgroundColor = "white";
    document.querySelector(".header_text").style.color = "black";
    document.querySelector(".menu-header").style.backgroundColor = "#35444f";
    document.querySelector(".bottom_container").style.backgroundColor = "#63696e";
  };
  
  const darkTheme = function () {
    themeBtn.innerHTML = "Light";
    themeBtn.classList.replace("light", "dark");
    document.body.style.backgroundColor = "black";
    document.querySelector(".container").style.backgroundColor = "black";
    themeBtn.style.color = "black";
    themeBtn.style.backgroundColor = "white";
    document.querySelector(".header_text").style.color = "orange";
    document.querySelector(".menu-header").style.backgroundColor = "orange";
    document.querySelector(".bottom_container").style.backgroundColor = "orange";
  };

  const themeBtn = document.querySelector(".change-theme-btn");
  themeBtn.addEventListener("click", () => {
    if (themeBtn.classList.contains("light")) {
      localStorage.setItem("theme", "dark");
      darkTheme();
    } else {
      localStorage.setItem("theme", "light")
      lightTheme();
    }
  });  

if (themeStorage === "dark") {
    darkTheme();
}
if (themeStorage === "light") {
    lightTheme();
}