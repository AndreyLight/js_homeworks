// 1) input не відбувається при введенні з клавіатури чи інших діях, якщо при цьому не змінюється значення текстового поля. Натискання клавіш (наприклад - ⇦, ⇨) в полі не викликають цю функцію

document.addEventListener("keydown", (e) => {
  const keyboardKey = document.querySelectorAll(".btn")
  keyboardKey.forEach((button) => {
    if (e.code === button.dataset.id) {
      button.style.backgroundColor = "blue";
    } else {
      button.style.backgroundColor = "black"
    }
  })
});