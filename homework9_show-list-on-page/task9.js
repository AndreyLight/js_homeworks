// 1 - Можна створити тег за допомогою document.createElement(), де в дужках методіу буде вказаний тег,
// або можна скористатись властивістю елемента innerHTML, яка може отримувати, або встановлювати HTLM теги
// також можна скористатись функцією insertAdjacentHTML
// 2 - Перший параметр функції визначає позицію елемента, що додається функцією, відносно елемента, що викликає функцію
// Може мати значення - beforebegin (до елемента), afterbegin (після "<" тега), beforeend (перед ">" тега), afterend (після елемента)
// 3 - Можна знайти елемент та скористатись методом remove(), передавши йому елемент, або ж звернутись до елемента і призначити йому значення ""

const array = [
  "Kharkiv",
  "Kiev",
  ["Borispol", "Irpin"],
  "Odessa",
  "Lviv",
  "Dnieper",
];

parent = document.body;

const showArray = function (array, parent) {
  const ulElement = document.createElement("ul");
  array.forEach((element) => {
    if (Array.isArray(element)) {
      const createNewUl = document.createElement("ul");
      element.forEach((e) => {
        const createLi = document.createElement("li");
        createLi.innerHTML = e;
        createNewUl.appendChild(createLi);
      });
      ulElement.appendChild(createNewUl);
    } else {
        const createLiChild = document.createElement("li");
        createLiChild.innerHTML = element;
        ulElement.appendChild(createLiChild);
      };
    })
    parent.append(ulElement);
  };


showArray(array, parent);

function clearPage () {
    return document.body.innerHTML = ''
}

setTimeout(clearPage, 3000)
