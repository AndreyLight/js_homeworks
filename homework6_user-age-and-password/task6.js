// 1 - Воно необхідне, що ми могли використовувати символи, які ми інакше не могли би ввести через специфіку синтаксису мови програмування (наприклад апостроф).
// 2 Існує два типи оголошення функції:
// -  function declaration - складається з ключового слова function, імені функції, параметрів функції в круглих скобках, інструкцій в фігурних скобках.
// - function expression - більш "короткий" варіант виклику функції, може також мати вигляд стрілочної функції
// 3 - цей процес позначає підняття змінних та функцій на вершину своїх областей видимості до виконання коду.
// Для функцій можна, наприклад, використати виклик функції вище, ніж буде її оголошення.
// В такому випадку під час компіляції буде спочатку "піднессення" оголошення функції догори, а потім виклик і виконання.
// Зі змінними працює подібно, але якщо, наприклад, викликати console.log() змінної раніше ніж вона буде визначена, то ми отримаємо помилку, так як все що буде використано зі змінними до надання їм значень буде underfined.

function createUser() {
   const userName = prompt("Whats your first name?")
   const userLastName = prompt("Whats your last name?")
   const userBirthday = prompt("Enter your birth date")
   const todayDate = new Date;
   const person = {
    firstName:userName,
    lastName:userLastName,
    birthday:userBirthday,
    getlogin(){
        return this.firstName.charAt(0).toLowerCase() + this.lastName.toLowerCase()
    },
    getAge(){
    return todayDate.getFullYear() - this.birthday.slice(-4)
   },
   getPassword() {
    return this.firstName.charAt(0).toUpperCase() + this.lastName.toLowerCase() + this.birthday.slice(-4)
}
}
   return person
} 
const newUser = createUser()

console.log(newUser)
console.log(newUser.getlogin())
console.log(newUser.getAge())
console.log(newUser.getPassword())
