// 1 - даний метод виконує певну якусь певну функцію для кожного елемента массиву, послідовно перебираючи їх.
// Можна такоє встановити параметри перебору массива - елемент, ідекс, сам массив
// 2 - Очистити массив можна за допомогою властивості length:
// array = [1,2,3,4]
// array.length = 0 // пустий массив
// або через присвоєння нового пустого массива
// let  a1 = [1, 2, 3, 4]
// a1 = [] // m1.length = 0
// 3 - за допомогою метода Array.isArray(), який поверне відповедне булеве значення true (якщо змінна - массив), або false

const array = ['hello', 'world', 23, '23', null];
const typeOfData = "string"
function filterBy(array) {
return array.filter((element) => typeof element !== typeOfData) 
}

console.log(filterBy(array))