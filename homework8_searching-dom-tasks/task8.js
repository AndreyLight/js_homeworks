// 1 - DOM-дерево це об'єктне відображення HTML.Створюється на етапі парсингу сторінки браузера
// 2 - innerHTML має можливість отримувати HTML-розмітку всередині обраного елементу, а innerText буде отримувати лише текстовий контент елемента
// Наприклад HTML-код:
// <h2><b>Dobry dzien, everybody</b></h2>
// при спробі виклику document.querySelector("h1").innerHTML отримаємо <b>Dobry dzien, everybody</b>
// при спробі виклику document.querySelector("h1").innerText отримаємо Dobry dzien, everybody
// 3 - можна звернутись до елемента сторінки за допомогою таких методів пошуку - getElementById, getElementsByClassName, querySelector,
// querySelectorAll.
// Кращого способу немає, кожен з методів зручний та корисних в певних ситуаціях (моя суб'єктивна думка). 

const paragraph = document.querySelectorAll("p");
paragraph.forEach((e) => (e.style.backgroundColor = "#ff0000"));

const optList = document.getElementById("optionsList");
console.log(optList);
console.log(optList.parentElement);
optList.childNodes.forEach((val) =>
  console.log(val.nodeType + " " + val.nodeName)
);
console.dir(optList);

const testPar = document.getElementById("testParagraph");
testPar.textContent = "This is a paragraph";
console.dir(testPar);
// немає такого классу - testParagraph )

const liAndClass = document.querySelectorAll(".main-header li");
console.log(liAndClass);
liAndClass.forEach((e) => e.className = "nav-item");
console.dir(liAndClass);

const sectionTitleClass = document.querySelectorAll(".section-title");
sectionTitleClass.forEach((e) => e.classList.remove("section-title"));
console.dir(sectionTitleClass)
